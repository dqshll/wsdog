<?php
use Workerman\Worker;
require_once __DIR__ . '/Workerman/Autoloader.php';
require_once './StatEvent.php';

//Worker::$pidFile = __DIR__ . '/process.pid'; //进程状态
//Worker::$stdoutFile = __DIR__ . '/stdout.log'; // echo, vardump日志

$box_socket_id = 9;
$global_bird_uid = 100;
$box_connection=null;
define(QUIT_MARK, -44);

// 创建一个文-本协议的Worker监听2347接口
$wechat_worker = new Worker("websocket://0.0.0.0:2345");

$wechat_worker->count = 1;

// 当客户端连上来时分配uid，并保存连接，并通知所有客户端
$wechat_worker->onConnect = function ($connection)
{
    global $global_bird_uid;
    $connection->uid = ++$global_bird_uid;
    echo "+ player join -> " . $connection->uid;
};

function getSysCurTime() {
    list($t1, $t2) = explode(' ', microtime());
    return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
}


// 当客户端发送消息过来时，转发给主机
$wechat_worker->onMessage = function ($connection, $data) {
    global $box_socket_id, $box_connection, $wechat_worker;

    if ($data == "box") {
        echo "box connected!";
        $connection->uid = $box_socket_id;
        $box_connection = $connection;
        echo "box uid " . $connection->uid;
    } else if($box_connection === $connection) { // msg from box
        echo "msg from box = " . $data;
        $splits = explode('-', $data);

        if(!empty($splits)) {
            $n = count($splits);
//            echo "split_n=$n";
            if($n == 2) {
                if ($splits[1] == 'kick') {
                    foreach ($wechat_worker->connections as $conn) {

                        if ($conn->uid == $splits[0]) {
                            $conn->uid = QUIT_MARK;
                            $conn->statEvent->setEndTime(0);
                            $conn->close('bye');
                            echo "kick " . $splits[0];
                            break;
                        }
                    }
                } else {//if ($splits[1] == 'p') {
                    foreach ($wechat_worker->connections as $conn) {

                        if ($conn->uid == $splits[0]) {
                            $conn->send($splits[1]);
                            echo "forward to " . $splits[0] ." ". $splits[1];
                            $conn->statEvent->pareMsgForLog($splits[1]);
                            break;
                        }
                    }
                }
            } else if ($n == 1) {
//                echo "not from mobiles";
                if ($data == 'heart') {
                    $showtime=date("Y-m-d H:i:s");
                    echo "[$showtime box heart beating]";
                    $box_connection->send('beat');
                }
            }
        }
    } else {
        $t0 = (int)substr($data,1);
        $t1 = getSysCurTime();
        echo "receiving player ($connection->uid) input = " . $data;
        global $box_connection;

        if($box_connection) {
            if (strpos($data, ';')) {
                $connection->send('ack');

                $startWithC = (strpos($data, 'c') === 0);
                $startWithR = (strpos($data, 'r') === 0);

                if ($startWithC || $startWithR) {
                    $event = new StatEvent($data, $startWithR);
                    $connection->statEvent = $event;
                }
            }

            $data = "{$connection->uid}-".$data;
            $box_connection->send($data);

            $t2 = getSysCurTime();
            echo "sent to box data = " . $data . "time_esc=" . ($t1 - $t0) . "@" . ($t2 - $t0);
        }
    }

    echo "msg exchange " . $data;
};

// 当客户端断开时，清除数据
$wechat_worker->onClose = function ($connection)
{
    global $box_connection;

    if($box_connection && $connection->uid != QUIT_MARK) {
        $data = "{$connection->uid}-q";
        $box_connection->send($data);
        echo "player quit sent to box =" . $data;
        if(isset($connection->statEvent)) {
            $connection->statEvent->setEndTime(1);
        }
    } else if ($box_connection == $connection){
        echo "box connection lost" . $connection->uid;
    }
};

Worker::runAll();