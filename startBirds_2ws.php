<?php
use Workerman\Worker;
require_once __DIR__ . '/Workerman/Autoloader.php';

// 创建一个Worker监听2346端口，使用websocket协议通讯, 这是主机的socket
$box_worker = new Worker("websocket://0.0.0.0:2346");
$box_connection = null;
// 启动1个进程对外提供服务
$box_worker->count = 1;


$box_worker->onConnect = function ($connection) {
    global $box_connection;
    echo "+ box connect ok!";
    $box_connection = $connection;
};

// 当收到客户端发来的数据后返回hello $data给客户端
$box_worker->onMessage = function($connection, $data)
{
    // 向客户端发送hello $data
//    $connection->send('w' . $data);
};

$global_bird_uid = 100;

// 创建一个文本协议的Worker监听2347接口
$wechat_worker = new Worker("websocket://0.0.0.0:2347");

// 启动3个进程, 支持多客户端, 16个玩家, 16个在等待队列
$wechat_worker->count = 3;

// 当客户端连上来时分配uid，并保存连接，并通知所有客户端
$wechat_worker->onConnect = function ($connection)
{
    global $global_bird_uid;
    // 为这个链接分配一个uid
    $connection->uid = ++$global_bird_uid;
    echo "+ player join -> " . $connection->uid;
};

// 当客户端发送消息过来时，转发给主机
$wechat_worker->onMessage = function ($connection, $data)
{
//    global $text_worker;
//    foreach($text_worker->connections as $conn)
//    {
//        $conn->send("user[{$connection->uid}] said: $data");
//    }

    global $box_connection;

    if(true) {
        print "sending msg to box";
        $box_connection->send($data);
    } else {
        print "box is offline";
    }

    echo "receiving player input= " . $data . "\n";
};

// 当客户端断开时，清除数据
$wechat_worker->onClose = function ($connection)
{
//    global $text_worker;
//    foreach($text_worker->connections as $conn)
//    {
//        $conn->send("user[{$connection->uid}] logout");
//    }
    echo "- player quit -> " . $connection->uid;

};

Worker::runAll();