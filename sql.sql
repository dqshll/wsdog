-- mysql -uroot -pe5cda60c7e

CREATE DATABASE stat DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `flip` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
  `user_id` varchar(16) DEFAULT NULL,
  `openid` varchar(128) NOT NULL,
  `nickname` varchar(128) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `sex` varchar(2) NOT NULL DEFAULT '',
  `ts_barcode` TIMESTAMP,
  `ts_wait` TIMESTAMP,
  `ts_start` TIMESTAMP,
  `ts_end` TIMESTAMP,
  `ts_retry` TIMESTAMP,
  `duration` FLOAT NOT NULL DEFAULT '0',
  `score` int(8) NOT NULL DEFAULT '0',
  `quit` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;