<?php
class StatEvent {

    var $user_id;
    var $openid;
    var $nickname;
    var $country;
    var $city;
    var $sex="";
    var $ts_barcode=0;
    var $ts_wait=0;
    var $ts_start=0;
    var $ts_end=0;
    var $ts_retry=0;
    var $duration=0;
    var $score=0;
    var $quit=0;

    public function StatEvent ($msg, $retry) {

        $strArray = explode(';', $msg);

        $this->setBarCodeTime();
        if ($retry) {
            $this->setRetryTime();
        }

        $this->nickname = $strArray[1];
        $this->openid = $strArray[3];
        $this->user_id = $strArray[4];
        $this->country = $strArray[5];
        $this->city = $strArray[6];
        $this->sex=$strArray[7];

    }

    private function setBarCodeTime(){
        $this->ts_barcode = $this->curSystime();
    }

    private function setWaitTime(){ //取早值
        if ($this->ts_wait == 0) {
            $this->ts_wait = $this->curSystime();
        }
    }

    private function setStartTime(){
        $this->ts_start = $this->curSystime();
    }

    public function setEndTime($quit){
        $this->ts_end = $this->curSystime();
        if ($this->ts_end > $this->ts_start) {
            $this->duration = floatval($this->ts_end - $this->ts_start) * 0.001;
        }
        $this->quit = $quit;
        $this->save2Db();
    }

    private function setRetryTime(){
        echo "setRetryTime";
        $this->ts_retry = $this->curSystime();
    }

    public function setScore($value){
        $this->score = $value;
    }

    public function pareMsgForLog($value){
        $first = substr( $value, 0, 1 );
        if ($first == 'p') {
            $this->setStartTime();
        } else if ($first == 'w') {
            $this->setWaitTime();
        } else if ($first == 'b') {
            $this->setStartTime();
        } else if ($first == 's') {
            $this->score = substr($value, 1, strlen($value) -1);
        }
    }

    private function toDTS($value) {
        if ($value === 0) {
            return '0';
        } else {
            return date("Y-m-d@H:i:s" , substr($value,0,10));
        }
    }

    private function save2Db () {
        $t0 = getSysCurTime();
        echo "saving db";

        $db_connection = mysql_connect('localhost','root','e5cda60c7e');

        mysql_query("set names 'utf8'"); //数据库输出编码

        mysql_select_db('stat'); //打开数据库


        $v_ts_barcode = $this->toDTS($this->ts_barcode);
        $v_ts_wait = $this->toDTS($this->ts_wait);
        $v_ts_start = $this->toDTS($this->ts_start);
        $v_ts_end = $this->toDTS($this->ts_end);
        $v_ts_retry = $this->toDTS($this->ts_retry);


        $sql = "insert into flip (user_id,openid,nickname,country,city,sex,ts_barcode,ts_wait,ts_start,ts_end,ts_retry,duration,score,quit) 
        values ('$this->user_id','$this->openid','$this->nickname','$this->country','$this->city','$this->sex',
        '$v_ts_barcode','$v_ts_wait','$v_ts_start','$v_ts_end','$v_ts_retry','$this->duration','$this->score','$this->quit')";
//        echo $sql;
        mysql_query($sql);

        mysql_close(); //关闭MySQL连接
        $t1 = getSysCurTime();
        echo "db ok time_db = " . ($t1 - $t0);
    }

    private  function curSystime() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
    }

}
?>